from django.conf.urls import patterns, include, url
from django.contrib import admin

import settings
import plugins

admin.autodiscover()

urlpatterns = patterns('',
	url(r'^', include('plugins.urls')),
	url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^page/', include('plugins.page.urls')),
)


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    )
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^media/static/(?P<path>.*)$', 'serve'),
    )