# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('status', '0003_auto_20150115_1309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requision',
            name='date_get',
            field=models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u043f\u0440\u0438\u0435\u043c\u0430', blank=True),
            preserve_default=True,
        ),
    ]
