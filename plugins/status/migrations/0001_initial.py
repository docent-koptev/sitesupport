# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Requision',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=250, verbose_name='\u041a\u043e\u0434')),
                ('people', models.CharField(max_length=350, verbose_name='\u0424\u0418\u041e \u0437\u0430\u043a\u0430\u0437\u0447\u0438\u043a\u0430')),
                ('product', models.CharField(max_length=400, verbose_name='\u0427\u0442\u043e \u0432 \u0440\u0435\u043c\u043e\u043d\u0442\u0435')),
                ('articul', models.CharField(max_length=300, null=True, verbose_name='\u0410\u0440\u0442\u0438\u043a\u0443\u043b \u0442\u043e\u0432\u0430\u0440\u0430', blank=True)),
                ('date', models.DateField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043f\u0440\u0438\u0435\u043c\u0430')),
                ('status', models.BooleanField(default=False, verbose_name='\u0413\u043e\u0442\u043e\u0432 \u043a \u0432\u044b\u0434\u0430\u0447\u0435')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0430',
                'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438',
            },
            bases=(models.Model,),
        ),
    ]
