# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('status', '0002_auto_20150115_1250'),
    ]

    operations = [
        migrations.RenameField(
            model_name='requision',
            old_name='date',
            new_name='date_get',
        ),
    ]
