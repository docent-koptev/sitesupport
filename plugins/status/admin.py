# -*- coding: utf-8 -*-
# django imports
from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from plugins.status.models import *


class StatusAdmin(admin.ModelAdmin):
	list_display = ["code", 'people', "product", 'status', 'date_get','current_status','price', 'bug', 'location',]
	list_editable = ["status",'current_status', ]
	fieldsets = (
		('Информация о заказе',{
		    'fields': (('code', 'date_get'),('product', 'articul'), 'complect', 'bug', 'location',)
		}),
		('Информация о клиенте',{
		    'fields': (('people', 'phone'), 'talon',)
		}),
		('Информация о выдаче и готовности',{
		    'fields': ('current_status', 'finish_word', 'price', 'status', )
		}),
	)
admin.site.register(Requision,StatusAdmin)