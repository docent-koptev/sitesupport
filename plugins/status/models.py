# -*- coding: utf-8 -*-
# django imports
from django.db import models

class Requision(models.Model):
    code = models.CharField(u'Квитанция (код)',max_length=250)
    product = models.CharField(u'Что в ремонте',max_length=400)
    articul = models.CharField(u'Серийный номер товара',max_length=300,blank=True,null=True)
    complect = models.CharField(u'Комплект',max_length=350,blank=True)
    people = models.CharField(u'ФИО заказчика',max_length=350,blank=True)
    date_get = models.DateField(u'Дата приема',blank=True)
    phone = models.CharField(u'Телефон заказчика',max_length=350,blank=True)
    talon = models.CharField(u'Гарантийный талон',max_length=350,blank=True)
    bug = models.CharField(u'Неисправность',max_length=350,blank=True)
    location = models.CharField(u'Нахождение',max_length=350,blank=True)
    current_status = models.CharField(u'Состояние (для клиента)',max_length=350,blank=True)
    finish_word = models.CharField(u'Заключение',max_length=350,blank=True)
    price = models.CharField(u'Цена ремонта',max_length=350,blank=True)
    status = models.BooleanField(u'Готов к выдаче',default=False)
    
    class Meta:
        verbose_name = u"Заявка"
        verbose_name_plural = u'Заявки'

    def __unicode__(self):
        return u'%s: %s' % ( self.code, self.people)