# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField

class Title(models.Model):
    title = models.CharField(_('title'), max_length=255, blank=True)

    def __unicode__(self):
        return "(TitleBlock) %s" % self.title

class Text(models.Model):
    text = models.TextField(_('text'), blank=True)

    def __unicode__(self):
        return "(TextBlock) %s..." % self.text[:20]

class Image(models.Model):
    image = ImageField(_('image'), upload_to='gblocks/', blank=True)

    def __unicode__(self):
        return "(ImageBlock) %s" % self.image

class TitleAndText(models.Model):
    title = models.CharField(_('title'), max_length=255, blank=True)
    text = models.TextField(_('text'), blank=True)

    def __unicode__(self):
        return "(TitleAndTextBlock) %s" % self.title

class TitleTextAndImage(models.Model):
    title = models.CharField(_('title'), max_length=255, blank=True)
    text = models.TextField(_('text'), blank=True)
    image = ImageField(_('image'), upload_to='gblocks/', blank=True)

    def __unicode__(self):
        return "(TitleTextAndImageBlock) %s" % self.title

class Banner(models.Model):
    title = models.CharField(_('title'), max_length=255, blank=True)
    image = ImageField(_('image'), upload_to='gblocks/', blank=True, help_text='ширина изображения 320 пикселей')
    link = models.CharField(_('link'), max_length=255, blank=True)
    text = models.TextField(_('text'), blank=True)

    def __unicode__(self):
        return "(BannerBlock) %s" % self.title

class Upload(models.Model):
    file = models.FileField(_('File'), upload_to='file', blank=True)

    def __unicode__(self):
        return "(FileBlock) %s" % self.file

class Seo(models.Model):
    title = models.CharField(_('title'), max_length=255, blank=True)
    description = models.CharField(_('description'), max_length=255, blank=True)
    keywords = models.CharField(_('description'), max_length=255, blank=True)

    def __unicode__(self):
        return "(SeoBlock) %s" % self.title
#
#class TitleTextAndClass(models.Model):
#    title = models.CharField(_('title'), max_length=255, blank=True)
#    text = models.TextField(_('text'), blank=True, help_text=_(u'Тут должен быть html-список.'))
#    class_name = models.CharField(_('class'), max_length=255, blank=True)
#
#    def __unicode__(self):
#        return "(TitleTextAndClassBlock) %s" % self.title
