# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from plugins.gblocks.models import *
from django_generic_flatblocks.models import GenericFlatblock
from sorl.thumbnail.admin import AdminImageMixin
from django.conf import settings
from django import forms

class DisableDeleteFormSet(generic.BaseGenericInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(DisableDeleteFormSet, self).__init__(*args, **kwargs)
        self.can_delete = False

class GenericFlatblockInline(generic.GenericTabularInline):
    model = GenericFlatblock
    ct_field = 'content_type'
    ct_fk_field = 'object_id'
    extra = 1
    max_num = 1
    readonly_fields = ['slug']
    formset = DisableDeleteFormSet

class AbstractAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ['__unicode__', 'get_slug']
    inlines = [GenericFlatblockInline,]
    formfield_overrides = {models.TextField: {'widget':forms.Textarea(attrs={'class':'ckeditor'})}}

    def get_slug(self, obj):
        content_type = ContentType.objects.get_for_model(obj)
        flatblock = GenericFlatblock.objects.get(object_id=obj.pk, content_type = content_type)
        return flatblock.slug
    get_slug.short_description = u'уникальный код'

admin.site.register(Title, AbstractAdmin)
admin.site.register(Text, AbstractAdmin)
admin.site.register(Image, AbstractAdmin)
admin.site.register(Upload, AbstractAdmin)
admin.site.register(TitleAndText, AbstractAdmin)
admin.site.register(TitleTextAndImage, AbstractAdmin)
admin.site.register(Banner, AbstractAdmin)
admin.site.register(Seo, AbstractAdmin)
#admin.site.register(TitleTextAndClass, AbstractAdmin)
