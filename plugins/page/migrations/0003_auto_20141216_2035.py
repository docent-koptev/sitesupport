# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0002_action_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='actiongroup',
            name='description',
            field=models.CharField(default=1, max_length=100, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='actiongroup',
            name='name',
            field=models.CharField(unique=True, max_length=100, verbose_name='\u0418\u043c\u044f', blank=True),
            preserve_default=True,
        ),
    ]
