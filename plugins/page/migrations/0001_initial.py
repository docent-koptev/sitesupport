# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=40, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True)),
                ('link', models.CharField(help_text=b'\xd0\xbc\xd0\xbe\xd0\xb6\xd0\xbd\xd0\xbe \xd0\xb8\xd1\x81\xd0\xbf\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xb7\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd1\x8c \xd1\x88\xd0\xb0\xd0\xb1\xd0\xbb\xd0\xbe\xd0\xbd reverse__["url name", (arg1, arg2), {"kwarg1": kwarg1} ]', max_length=100, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True)),
                ('active', models.BooleanField(default=False, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e\u0441\u0442\u044c')),
                ('position', models.IntegerField(default=999, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
            ],
            options={
                'ordering': ('position',),
                'verbose_name': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442 \u043c\u0435\u043d\u044e',
                'verbose_name_plural': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442\u044b \u043c\u0435\u043d\u044e',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ActionGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, verbose_name=b'\xd0\x98\xd0\xbc\xd1\x8f', blank=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': '\u041c\u0435\u043d\u044e',
                'verbose_name_plural': '\u041c\u0435\u043d\u044e',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('slug', models.CharField(max_length=100, verbose_name='\u0421\u043b\u0443\u0433')),
                ('short_text', models.TextField(blank=True)),
                ('body', models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442', blank=True)),
                ('active', models.BooleanField(default=False, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e\u0441\u0442\u044c')),
                ('exclude_from_navigation', models.BooleanField(default=False, verbose_name='Exclude from navigation')),
                ('position', models.IntegerField(default=999, verbose_name='Position')),
                ('file', models.FileField(upload_to=b'files', verbose_name='File', blank=True)),
            ],
            options={
                'ordering': ('position',),
                'verbose_name': '\u0442\u0435\u043a\u0441\u0442\u043e\u0432\u0430\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430',
                'verbose_name_plural': '\u0442\u0435\u043a\u0441\u0442\u043e\u0432\u044b\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='action',
            name='group',
            field=models.ForeignKey(related_name='actions', verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430', to='page.ActionGroup'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='action',
            name='page',
            field=models.ForeignKey(related_name='actions', verbose_name='\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430', blank=True, to='page.Page', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='action',
            name='parent',
            field=models.ForeignKey(verbose_name='\u0420\u043e\u0434\u0438\u0442\u0435\u043b\u044c', blank=True, to='page.Action', null=True),
            preserve_default=True,
        ),
    ]
