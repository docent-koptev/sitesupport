# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.db import models
from django.conf import settings

#from rollyourown.seo.admin import register_seo_admin, get_inline
#from docent.seo import SEOMetadata
from models import *
from mptt.admin import MPTTModelAdmin as FeinCMSModelAdmin
from ckeditor.widgets import CKEditorWidget
#register_seo_admin(admin.site, SEOMetadata)

class PageAdminForm(forms.ModelForm):
        body = forms.CharField(widget=CKEditorWidget(),label=u'Текст')
        class Meta:
            model = Page

class PageAdmin(admin.ModelAdmin):
    form = PageAdminForm
    prepopulated_fields = prepopulated_fields = {"slug": ("title",)}
   # inlines = [get_inline(SEOMetadata)]

admin.site.register(Page, PageAdmin)

class ActionInline(admin.TabularInline):
    model = Action
    extra = 1

class ActionGroupAdmin(admin.ModelAdmin):
    list_display = 'description','name',
    list_display_links = 'description',
   # readonly_fields = ('name',)
    inlines = [ActionInline,]

admin.site.register(ActionGroup, ActionGroupAdmin)

class ActionAdmin(FeinCMSModelAdmin):
    list_filter = ('group',)
    list_display = ('__unicode__', 'active', 'group',)
    list_editable = ('active', 'group',)

admin.site.register(Action, ActionAdmin)