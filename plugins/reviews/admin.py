# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.db import models
from django import forms
from models import Review
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin
from ckeditor.widgets import CKEditorWidget


class ReviewAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = [ 'body', 'who', 'creation_date']
    search_fields = ['body',]
    list_filter = ['display',]
    date_hierarchy = 'creation_date'


    fieldsets = (
        (None,{
            'fields': ('image', 'who','body','display', ('creation_date',) )
        }),
    )

    def admin_image_preview(self, obj):
        if obj.image:
            thumbnail = get_thumbnail(obj.image, '70')
            return '<a href="%s" target="_blank"><image style="max-height:70px;max-width:70px" src="%s"/></a>' % (obj.image.url, thumbnail.url)
        return ''
    admin_image_preview.short_description = u'превью'
    admin_image_preview.allow_tags = True


admin.site.register(Review, ReviewAdmin)
