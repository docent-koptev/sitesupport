# -*- coding: utf-8 -*-
import datetime
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField


class Review(models.Model):
	title = models.CharField(_(u'заголовок'), max_length=255,blank=True)
	body = models.TextField(_(u'Тело отзыва'), blank=True)
	who = models.CharField(_(u'От кого отзыв'), max_length=255)
	image = ImageField(_(u'изображение'), upload_to='uploads/reviews', blank=True)
	display = models.BooleanField(_(u'опубликовать'), default=True)
	creation_date = models.DateTimeField(_(u'дата публикации'), blank=True)	
	#last_modification = models.DateTimeField(_(u'дата последнего изменения'), blank=True)

	class Meta:
		ordering = ('-creation_date',)
		verbose_name = _(u'Отзыв')
		verbose_name_plural = _(u'Отзывы')

	def __unicode__(self):
		return self.title
