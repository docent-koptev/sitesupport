# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.db import models
from django import forms
from models import News
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin
from ckeditor.widgets import CKEditorWidget


class NewsAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ['title', 'admin_image_preview', 'display', 'last_modification', 'creation_date']
    list_editable = ['display',]
    search_fields = ['title', 'body', 'short']
    list_filter = ['display',]
    date_hierarchy = 'creation_date'
    readonly_fields = ['last_modification',]
    formfield_overrides = {
        models.TextField: {'widget':CKEditorWidget()}
    }
    prepopulated_fields = {'slug': ['title',]}
    fieldsets = (
        (None,{
            'fields': ('title', 'slug', 'body', 'short', 'image', 'display', ('creation_date', 'last_modification') )
        }),
    )

    def admin_image_preview(self, obj):
        if obj.image:
            thumbnail = get_thumbnail(obj.image, '70')
            return '<a href="%s" target="_blank"><image style="max-height:70px;max-width:70px" src="%s"/></a>' % (obj.image.url, thumbnail.url)
        return ''
    admin_image_preview.short_description = u'превью'
    admin_image_preview.allow_tags = True


admin.site.register(News, NewsAdmin)