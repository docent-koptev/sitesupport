# -*- coding: utf-8 -*-
from django.db import models
from django.forms import ModelForm
from django import forms



class ContactsForm(forms.Form):
	name = forms.CharField(label=u'Ваше имя', max_length=100,required=True)
	email = forms.CharField(label=u'Ваш email', max_length=100,required=True)
	text = forms.CharField(label=u'Сообщение', max_length=100,required=True)
