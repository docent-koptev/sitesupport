# -*- coding: utf-8 -*-
# django imports
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField

class Slider(models.Model):
    """ Слайдер
    """
    title = models.CharField(_(u'заголовок'), blank=True, max_length=250, help_text=_(u'Рабочее название для админки.'))
    show_on = models.CharField(_(u'показывать на'), default=u'main', max_length=250,
                               help_text=_(u'принадлежность к странице, на которой будет находиться слайд'),
                               choices=(("main", "главной странице (982×430)"),))
    image = ImageField(_(u'изображение'), upload_to='slider')
    name = models.CharField(_(u'Первая строка'), blank=True, max_length=250, help_text=_(u'Пр.:Супер крутой тариф'))
    url = models.CharField(_(u'ссылка'), blank=True, max_length=10000)
    target = models.BooleanField(_(u"открывать ссылку в новом окне"), default=False)
    display = models.BooleanField(_(u'отображать'), default=True)
    position = models.IntegerField(_(u'позиция'), default=100, help_text=_(u'номер для сортировки слайдов'))
    bonus = models.CharField(_(u'Бонусы'), blank=True, max_length=10000,help_text=u'Выводится справа')
    is_connect = models.BooleanField(_(u"Показ ссылки 'Подключиться'"), default=False)
    two_str = models.CharField(_(u'Вторая строка слайда'), blank=True, max_length=250, help_text=_(u'Пр.:цена от 2500 рублей'))
    three_str = models.CharField(_(u'Третья строка'), blank=True, max_length=250, help_text=_(u'Пр.:intel core 2 duo 1500'))
    
    class Meta:
        ordering = ("-position", "show_on", )
        verbose_name = u"слайд"
        verbose_name_plural = u'слайды'

    def __unicode__(self):
        return u'%s: %s' % ( self.show_on, self.title)