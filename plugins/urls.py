# -*- coding: utf-8 -*-
from django.conf.urls import *


urlpatterns = patterns('plugins.views',
	url(r'^$', 'home', name = 'home'),
	url(r'^check$', 'check_status', name = 'check_status'),
	url(r'^feedback$', 'feedback', name = 'feedback'),
	
)

urlpatterns += patterns('',
	url(r'^page/', include('plugins.page.urls')),
	url(r'^news/', include('plugins.news.urls')),

)