# -*- coding: utf-8 -*-
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.contrib.auth import login, authenticate
from django import forms
from django.shortcuts import get_object_or_404, redirect, render,HttpResponse
from django.views.decorators.csrf import requires_csrf_token
from annoying.decorators import render_to
from annoying.functions import get_object_or_None

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.mail import send_mail

from plugins.status.models import Requision
from plugins.forms import ContactsForm
import json


@render_to(template='home/home.html')
def home(request):
    return locals()


def check_status(request):
	if 'status' in request.GET:
		code = request.GET['code']
		if code:
			requision = get_object_or_None(Requision,code=code)
			if requision is not None:
				if requision.status:
					return HttpResponse(json.dumps({"result":"ready"}))
				else:
					return HttpResponse(json.dumps({"result":'notready',"status":requision.current_status }))

	return HttpResponse(json.dumps({"result":"error"}))


def feedback(request):
	if 'feedback' in request.POST:
		form = ContactsForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			name = cd['name']
			email = cd['email']
			text = cd['text']
			body = u'Сообщение от: ' + name
			body += u'\nПочта: ' + email
			body += u'\nТекст:' + text
 			send_mail(u'Заявка с сайта', body, 'no-reply@support.ru', ['akoptev1989@yandex.ru'], fail_silently=False)
			return HttpResponse(json.dumps({"result":"success"}))
		else:
			errors = {}
			for k in form.errors:
			    # Теоретически у поля может быть несколько ошибок...
			    errors[k] = form.errors[k][0]
			return HttpResponse(json.dumps({"errors":errors,"result":"error"}))


				