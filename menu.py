# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from admin_tools.menu import items, Menu
from django_generic_flatblocks.models import GenericFlatblock


def get_gblock(slug):
    try:
        return GenericFlatblock.objects.get(slug=slug).content_object
    except Exception as ex:
        return None

def get_gblock_admin_url(slug):
    related_object = get_gblock(slug)
    if related_object:
        app_label = related_object._meta.app_label
        module_name = related_object._meta.module_name
        return '/admin/%s/%s/%s/' % (app_label, module_name, related_object.pk)
    else:
        return '#'

class CustomMenu(Menu):
    """
    Custom Menu for apple admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            items.Bookmarks(),
            items.ModelList(
                u'Сайт',
                models=(','),
                children = [
                        items.MenuItem(u'Заголовки блоков',
                        children = [
                            items.MenuItem(
                                u'О нас',
                                get_gblock_admin_url('about_us2'),
                            ),
                            items.MenuItem(
                                u'Что умеем текст',
                                get_gblock_admin_url('features'),
                            ),

                            items.MenuItem(
                                u'Прайс-лист описание',
                                get_gblock_admin_url('price_list'),
                            ),
                            items.MenuItem(
                                u'Прайс-лист документ',
                                get_gblock_admin_url('uploads'),
                            ),
                            items.MenuItem(
                                u'Статус заявки',
                                get_gblock_admin_url('status'),
                            ),

                            items.MenuItem(
                                u'Написать нам',
                                get_gblock_admin_url('feedback'),
                            ),
                            items.MenuItem(
                                u'Отзывы',
                                get_gblock_admin_url('reviews'),
                            ),
                            items.MenuItem(
                                u'SEO',
                                get_gblock_admin_url('seo_block'),
                            ),

                        ]
                    ),
                    items.MenuItem(u'О нас',
                        children = [
                            items.MenuItem(
                                u'О нас текст 1',
                                get_gblock_admin_url('first_t'),
                            ),

                            items.MenuItem(
                                u'О нас текст 2',
                                get_gblock_admin_url('first_t_2'),
                            ),

                            items.MenuItem(
                                u'О нас текст 3',
                                get_gblock_admin_url('first_t_3'),
                            ),
                            
                            items.MenuItem(
                                u'О нас текст 4',
                                get_gblock_admin_url('first_t_4'),
                            ),
                            
                        ]
                    ),
                    items.MenuItem(u'Наши услуги',
                        children = [
                            items.MenuItem(
                                u'Первый текст',
                                get_gblock_admin_url('feat_1'),
                            ),
                            items.MenuItem(
                                u'Второй текст',
                                get_gblock_admin_url('feat_2'),
                            ),

                            items.MenuItem(
                                u'Третий текст',
                                get_gblock_admin_url('feat_3'),
                            ),

                            items.MenuItem(
                                u'Четвертый текст',
                                get_gblock_admin_url('feat_4'),
                            ),
                            
                            items.MenuItem(
                                u'Пятый текст',
                                get_gblock_admin_url('feat_5'),
                            ),
                            
                            items.MenuItem(
                                u'Шестой текст',
                                get_gblock_admin_url('feat_6'),
                            ),

                        ]
                    ),
                ]
            ),
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)
