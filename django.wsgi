import os
import sys

sys.path.insert(0, '/home/appleinrus/webapps/support/ienv/lib/python2.7/site-packages')
sys.path.insert(0, '/home/appleinrus/webapps/support/sitesupport')

from django.core.wsgi import get_wsgi_application


os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

application = get_wsgi_application()