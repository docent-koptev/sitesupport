# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for apple.
    """
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        # append a link list module for "quick links"
        self.children.append(modules.LinkList(
            _('Quick links'),
            layout='inline',
            draggable=False,
            deletable=False,
            collapsible=False,
            children=[
                [_('Return to site'), '/'],
                [_('Change password'),
                 reverse('%s:password_change' % site_name)],
                [_('Log out'), reverse('%s:logout' % site_name)],
            ]
        ))

        self.children.append(modules.Group(
            title=u"Управление сайтом",
            display="tabs",
            children=[
                modules.AppList(
                    title=u'Слайды',
                    models=('plugins.slider.models.*',)
                ),
                modules.AppList(
                    title=u'Управление меню',
                    models=('plugins.page.models.ActionGroup','plugins.page.models.Action',)
                ),

                modules.AppList(
                    title='Новости и заявки',
                    models=('plugins.news.models.*','plugins.status.models.*',)
                ),

                modules.AppList(
                    title='Отзывы',
                    models=('plugins.reviews.models.*',)
                ),
            ]
        ))


       
        
        self.children.append(modules.Group(
            title=u"Администрирование",
            display="tabs",
            children=[
                modules.AppList(
                    title=u'Меню',
                    models=('page.models.ActionGroup','page.models.Action',)
                ),

                modules.AppList(
                    title='Контент',
                    models=('page.models.Page','news.models.News',)
                ),

                modules.AppList(
                    title='Администратор',
                    models=('django.contrib.*','chronograph.models.*',)
                ),

            ]
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(_('Recent Actions'), 10))



class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for apple.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
